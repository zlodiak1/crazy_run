from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Email, EqualTo
from app.models import User


class RegistrationForm(FlaskForm):
    username =  StringField('Имя пользователя', validators=[DataRequired()])
    email =     StringField('Email', validators=[DataRequired(), Email()])
    password =  PasswordField('Пароль', validators=[DataRequired()])
    password2 = PasswordField('Повтор пароля', validators=[DataRequired(), EqualTo('password')])
    desc =      TextAreaField('Дополнительная информация')
    submit =    SubmitField('Зарегистрироваться')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Этот email занят')


class LoginForm(FlaskForm):
    email =     StringField('Email', validators=[DataRequired(), Email()])
    password =  PasswordField('Пароль', validators=[DataRequired()])
    submit =    SubmitField('Войти')