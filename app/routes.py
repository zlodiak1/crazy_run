from flask import render_template, request, url_for, redirect, flash
from flask_login import current_user, login_user, logout_user, login_required

from app import app, db
from app.forms import RegistrationForm, LoginForm
from app.models import User


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Главная страница')


@app.route('/registration', methods=['GET', 'POST'])
def registration():
    registration_form = RegistrationForm()

    if request.method == 'POST' and registration_form.validate_on_submit():
        username = request.form['username']
        email = request.form['email']
        desc = request.form['desc']

        user = User(
            username=username,
            email=email,
            desc=desc
        )
        user.set_password(request.form['password'])

        try:
            db.session.add(user)
            db.session.commit()
            return redirect(url_for('index'))
        except Exception as e:
            db.session.rollback()
            flash('Ошибка операции записи в БД', e)

    return render_template('registration.html', title='Регистрация', form=registration_form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    print('current_user', current_user.is_authenticated)
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    login_form = LoginForm()

    if request.method == 'POST' and login_form.validate_on_submit():
        user = User.query.filter_by(email=request.form['email']).first()
        if user is None or not user.check_password(request.form['password']):
            flash('Неверные авторизационные данные')
            return redirect(url_for('login'))
        else:
            login_user(user, remember=True)
            flash('Вы вошли')
            return redirect(url_for('index'))

    return render_template('login.html', title='Вход', form=login_form)


@app.route('/logout')
def logout():
    logout_user()
    flash('Вы вышли')
    return redirect(url_for('index'))


@app.route('/page1')
@login_required
def page1():
    return render_template('page1.html', title='page1')


@app.route('/page2')
@login_required
def page2():
    return render_template('page2.html', title='page2')


@app.route('/users', methods=['GET'])
def get_users():
    return 'get all users'


@app.route('/users/<id>', methods=['GET'])
def get_user(id):
    return 'get user' + str(id)

        
@app.route('/users', methods=['POST'])
def create_user():
    return 'create new user'


@app.route('/users/<id>', methods=['PATCH'])
def update_user(id):
    return 'partially update user'


@app.route('/users/<id>', methods=['DELETE'])
def delete_user(id):
    return 'delete user' + str(id)




