import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'skldnfkui3489ywsefkfhnkrl45'
    SQLALCHEMY_DATABASE_URI='postgresql://cr_user:qwerty@localhost:5432/crazy_run'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
