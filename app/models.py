from app import db, login

import datetime

from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id =            db.Column(db.Integer, primary_key=True)
    username =      db.Column(db.String(40), unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    reg_date_utc =  db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow())
    upd_date_utc =  db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow())
    email =         db.Column(db.String(40))
    desc =          db.Column(db.Text)
    is_active =     db.Column(db.Boolean, default=False)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)    

    def __repr__(self):
        return '<User {}>'.format(self.username)

    @staticmethod
    def _bootstrap(count=10, locale='ru'):
        from mimesis import Generic, Person, Text

        generic = Generic(locale)
        person = Person(locale)
        text = Text(locale)

        for _ in range(count):
            user = User(
                username=person.full_name(),
                password_hash=generate_password_hash(generic.text.word()),
                reg_date_utc=datetime.datetime.utcnow(),
                upd_date_utc=datetime.datetime.utcnow(),
                email=person.email(),
                desc=text.text(5)
            )

            db.session.add(user)

            try:
                db.session.commit()
            except Exception:
                db.session.rollback()


@login.user_loader
def load_user(id):
    u = User.query.get(int(id))
    print('=====', u)
    return u